<br>
run in production

`java -Dspring.profiles.active=production -jar ccrpoutstanding-0.0.1-SNAPSHOT.jar`

<br>
in SIT

`java -Dspring.profiles.active=SIT -jar ccrpoutstanding-0.0.1-SNAPSHOT.jar`

<br>
in develop mode

`java -Dspring.profiles.active=develop -jar ccrpoutstanding-0.0.1-SNAPSHOT.jar`


<br>


run test with force date

`java -Dspring.profiles.active=develop -jar ccrpoutstanding-0.0.1-SNAPSHOT.jar 2018-10-24`