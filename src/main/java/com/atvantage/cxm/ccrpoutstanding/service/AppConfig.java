package com.atvantage.cxm.ccrpoutstanding.service;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(AppProperty.class)
public class AppConfig {

    private AppProperty props;

    public AppConfig(AppProperty props) {
        this.props = props;
    }

    public AppProperty getProps() {
        return props;
    }

    public void setProps(AppProperty props) {
        this.props = props;
    }
}
