package com.atvantage.cxm.ccrpoutstanding.service;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.validation.annotation.Validated;


@ConfigurationProperties(prefix = "appproperty")
@Validated
public class AppProperty {
    private String mongodbUsername;

    public String getMongodbUsername() {
        return mongodbUsername;
    }

    public void setMongodbUsername(String mongodbUsername) {
        this.mongodbUsername = mongodbUsername;
    }
}
