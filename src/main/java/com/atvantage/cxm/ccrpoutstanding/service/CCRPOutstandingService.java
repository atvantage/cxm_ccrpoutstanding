package com.atvantage.cxm.ccrpoutstanding.service;


import com.atvantage.cxm.ccrpoutstanding.data.Transaction;
import com.atvantage.cxm.ccrpoutstanding.data.TransactionMongoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@EnableConfigurationProperties(AppProperty.class)
public class CCRPOutstandingService {
    private static  final Logger log = LoggerFactory.getLogger(CCRPOutstandingService.class);

    private final TransactionMongoRepository repository;

    @Autowired
    private AppConfig appConfig;

    @Autowired
    public CCRPOutstandingService(TransactionMongoRepository repository) {
        this.repository = repository;
    }

    public List<Transaction> getCCRPOutstandingFromMongo(){
        log.debug("AppConfig = "+appConfig+", appConfig :"+appConfig.getProps().getMongodbUsername());
        Query query = new Query();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MONTH, Calendar.SEPTEMBER);
        cal.set(Calendar.DATE, 03);

        Date startDate = cal.getTime();
        cal.add(Calendar.DATE,2);
        Date endDate = cal.getTime();
        log.debug("startDate = "+startDate+", endDate = "+endDate);

        List<Transaction> ccrpTransactions =
                // repository.findBySource("CCRP");
                repository.findBySourceAndInputDateBetweenAndSourceReferenceIdIsNotNullOrderByInputDate("CCRP",startDate,endDate);
        return ccrpTransactions;
    }

    public List<Transaction> getCCRPTransactionWhereInputDateBetween(LocalDateTime startDate, LocalDateTime endDate){
        log.debug("AppConfig = "+appConfig+", appConfig :"+appConfig.getProps().getMongodbUsername());
        Query query = new Query();


        Date startDate2 = Date.from(startDate.atZone(ZoneId.of("UTC+00:00")).toInstant());
        log.debug("startDate2 = {}",startDate2);

        Date endDate2 = Date.from(endDate.atZone(ZoneId.of("UTC+00:00")).toInstant());
        log.debug("endDate2 = {}",endDate2);
        List<Transaction> ccrpTransactions =
            repository.findBySourceAndInputDateBetweenAndSourceReferenceIdIsNotNullOrderByInputDate("CCRP",startDate2, endDate2);
        return ccrpTransactions;
    }
}
