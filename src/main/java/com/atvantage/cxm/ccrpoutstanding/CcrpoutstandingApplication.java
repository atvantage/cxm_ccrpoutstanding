package com.atvantage.cxm.ccrpoutstanding;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

@Slf4j
@SpringBootApplication
public class CcrpoutstandingApplication implements CommandLineRunner {

    @Autowired
    private JobRunner3 jobRunner3;


    public static void main(String[] args) {
        SpringApplication.run(CcrpoutstandingApplication.class, args);


    }

    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    @Override
    public void run(String... args) throws Exception {
        String dateText=null;
        if(args.length>=1){
            dateText = args[0];
        }else {
            dateText = LocalDate.now().format(DateTimeFormatter.ISO_DATE);
        }

        String arg2FromZoneId = "UTC";
        String arg3ToZoneId = "UTC";
        if(args.length>=3){
            arg2FromZoneId = args[1];
            arg3ToZoneId = args[2];
        }
        log.debug("forceDate = {}, fromZoneId={}, toZoneId={}",dateText,arg2FromZoneId, arg3ToZoneId);
        jobRunner3.setDateText(dateText);
        jobRunner3.setFromZoneId(arg2FromZoneId);
        jobRunner3.setToZoneId(arg3ToZoneId);
        jobRunner3.run();
    }
}
