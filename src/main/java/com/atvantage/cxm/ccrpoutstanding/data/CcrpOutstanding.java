package com.atvantage.cxm.ccrpoutstanding.data;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;


@Entity
@Table(name="ccrp_outstanding")
@Data
public class CcrpOutstanding {

  @Id
  @Column(length = 20)
  private String sourceReferenceId;

  @Column(length = 3)
  private String status;

  @Column(length = 10)
  private String inputUser;

  private Date inputDate;

  @Column(length = 10)
  private String updateUser;

  private Date updateDate;

  @Column(length = 20)
  private String requestId;

  @Column(length = 20)
  private String requestChannel;

  @Column(length = 20)
  private String sequenceId;

  private Date receiveDateTime;

  private Date createSurveyDate;
  private String retryTime;

  @Column(length = 20)
  private String requestStatus;

  @Column(length = 100)
  private String requestDescription;

  @Column(length = 100)
  private String quotaResult;

  @Column(length = 20)
  private String source;

  @Column(length = 20)
  private String surveyType;

  @Column(length = 20)
  private String transactionType;

  @Column(length = 20)
  private String transactionTypeLevel;

  @Column(length = 30)
  private String surveySet;


  private String surveyUrl;

  private String smsMessage;

  @Column(length = 20)
  private String optIn;

  @Column(length = 60)
  private String alertCategories;

  @Column(length = 10)
  private String expireFlag;

  @Column(length = 10)
  private String entityType;

  @Column(length = 10)
  private String customerSubSegmentAbbreviation;

  @Column(length = 30)
  private String customerSubSegmentDescription;

  @Column(length = 10)
  private String wmCode;

  @Column(length = 20)
  private String responseChannel;

  @Column(length = 100)
  private String agentTeam;

  @Column(length = 10)
  private String staffId;

  @Column(length = 10)
  private String saleCode;

  @Column(length = 50)
  private String subChannel;

  @Column(length = 30)
  private String subChannel1;

  @Column(length = 30)
  private String subChannel2;

  @Column(length = 30)
  private String channelScript;

  @Column(length = 20)
  private String productCode;

  @Column(length = 250)
  private String productName;

  @Column(length = 20)
  private String productGroupAbbreviation;

  @Column(length = 200)
  private String productGroupDescription;

  @Column(length = 100)
  private String productSubGroupDescription;

  @Column(length = 50)
  private String productGroup;

  @Column(length = 20)
  private String productSubGroup;

  @Column(length = 20)
  private String productLevel;

  @Column(length = 20)
  private String nonProductIssue;

  @Column(length = 200)
  private String productScript;

  @Column(length = 50)
  private String subInteractionType;

  @Column(length = 200)
  private String interactionScript;

  @Column(length = 50)
  private String activityType;

  @Column(length = 10)
  private String branchCodeRaw;

  @Column(length = 200)
  private String branchName;

  @Column(length = 20)
  private String zoneCode;

  @Column(length = 20)
  private String zoneCode2;

  @Column(length = 10)
  private String zoneCodeRaw;

  @Column(length = 200)
  private String zoneName;

  @Column(length = 20)
  private String regionCode;

  @Column(length = 20)
  private String regionCode2;

  @Column(length = 10)
  private String regionCodeRaw;

  @Column(length = 200)
  private String regionName;

  @Column(length = 20)
  private String niceBranchCode;

  @Column(length = 200)
  private String niceBranchName;

  @Column(length = 20)
  private String niceZoneCode;

  @Column(length = 20)
  private String niceRegionCode;

  @Column(length = 50)
  private String productCategory;

  @Column(length = 50)
  private String subCategory;


  private Integer fcr;

  @Column(length = 50)
  private String responsibleBuGrouping;

  @Column(length = 100)
  private String responsibleBuDetails;

  @Column(length = 20)
  private String callDriver;

  @Column(length = 20)
  private String callWaiting;

  @Column(length = 20)
  private String callLength;

  @Column(length = 50)
  private String campaignCode;

  @Column(length = 50)
  private String parentBusinessOutcome;

  @Column(length = 50)
  private String businessOutcomeGrouping;

  @Column(length = 20)
  private String resultStatus;

  @Column(length = 20)
  private String spendTimeInSystem;

  @Column(length = 20)
  private String activityHistory;

  @Column(length = 20)
  private String activityTopUp;

  @Column(length = 100)
  private String platformUse;

  @Column(length = 100)
  private String deviceModel;

  @Column(length = 100)
  private String applicationVersion;


  private Double probability;
  private Double accumulateProbability;
  private Integer assigned;
  private Integer quota;
  private Integer successCount;

  private Boolean picked;

  private Date requestDateTime;

  @Column(length = 2)
  private String language;

  @Column(length = 50)
  private String customerType;

  @Column(length = 50)
  private String customerId;

  @Column(length = 50)
  private String customerName;

  @Column(length = 5)
  private String customerSegment;

  @Column(length = 5)
  private String customerSubSegment;

  @Column(length = 50)
  private String channel;

  @Column(length = 20)
  private String productGroupCode;

  @Column(length = 20)
  private String interactionCode;

  @Column(length = 50)
  private String interactionType;

  private Date interactionDateTime;

  @Column(length = 100)
  private String activityDescription;

  @Column(length = 20)
  private String branchCode;

  @Column(length = 200)
  private String complaintIssue;

  @Column(length = 200)
  private String complaintInputChannel;

  @Column(length = 50)
  private String complaintClassificationLevel;


  private Integer overSla;

  @Column(length = 20)
  private String responsibleBu1;

  @Column(length = 20)
  private String responsibleBu2;


  private Boolean isPicked;

  private Date openDate;

  private Integer sla;


  private Date activateDateTime;

  private Boolean isVerbatim;

  private Date loginDateTime;

  private Date logOutDateTime;

  private Date sellingDate;

  private String responsibleChannel;

  private String responsibleSubChannel;


}
