package com.atvantage.cxm.ccrpoutstanding.data;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface TransactionMongoRepository extends MongoRepository<Transaction,String> {
    public List<Transaction> findBySource(String source);
    public List<Transaction> findByInputDateBetween(Date startDate, Date endDate);
    public List<Transaction> findBySourceAndInputDateBetweenAndSourceReferenceIdIsNotNullOrderByInputDate(String source, Date startDate, Date endDate);


}
