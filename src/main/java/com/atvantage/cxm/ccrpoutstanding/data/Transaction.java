package com.atvantage.cxm.ccrpoutstanding.data;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;

@Data
@Document(collection = "Transaction")
public class Transaction {

    @Id
    private String _id;

    private String status;

    private String inputUser;

    private Date inputDate;

    private String updateUser;

    private Date updateDate;

    private String requestId;

    private String requestChannel;

    private String sequenceId;

    private Date receiveDateTime;


    private Date createSurveyDate;
    private String retryTime;

    private String requestStatus;

    private String requestDescription;

    private String quotaResult;

    private String source;

    private String surveyType;

    private String transactionType;

    private String transactionTypeLevel;

    private String surveySet;


    private String surveyUrl;

    private String smsMessage;

    private String optIn;

    private String alertCategories;

    private String expireFlag;

    private String entityType;

    private String customerSubSegmentAbbreviation;

    private String customerSubSegmentDescription;

    private String wmCode;

    private String responseChannel;

    private String agentTeam;

    private String staffId;

    private String saleCode;

    private String subChannel;

    private String subChannel1;

    private String subChannel2;

    private String channelScript;

    private String productCode;

    private String productName;

    private String productGroupAbbreviation;

    private String productGroupDescription;

    private String productSubGroupDescription;

    private String productGroup;

    private String productSubGroup;

    private String productLevel;

    private String nonProductIssue;

    private String productScript;

    private String subInteractionType;

    private String interactionScript;

    private Date interactionDateTime;

    private String activityType;

    private String branchCodeRaw;

    private String branchName;

    private String zoneCode;

    private String zoneCode2;

    private String zoneCodeRaw;

    private String zoneName;

    private String regionCode;

    private String regionCode2;

    private String regionCodeRaw;

    private String regionName;

    private String niceBranchCode;

    private String niceBranchName;

    private String niceZoneCode;

    private String niceRegionCode;

    private String productCategory;

    private String subCategory;


    private Integer fcr;

    private String responsibleBuGrouping;

    private String responsibleBuDetails;

    private String callDriver;

    private String callWaiting;

    private String callLength;

    private String campaignCode;

    private String parentBusinessOutcome;

    private String businessOutcomeGrouping;

    private String resultStatus;

    private String spendTimeInSystem;

    private String activityHistory;

    private String activityTopUp;

    private String platformUse;

    private String deviceModel;

    private String applicationVersion;


    private Double probability;
    private Double accumulateProbability;
    private Integer assigned;
    private Integer quota;
    private Integer successCount;
    private boolean picked;
    private Date requestDateTime;

    private String language;

    private String customerType;

    private String customerId;

    private String customerName;

    private String customerSegment;

    private String customerSubSegment;

    private String sourceReferenceId;

    private String channel;

    private String productGroupCode;

    private String interactionCode;

    private String interactionType;

    private String activityDescription;

    private String branchCode;

    private String complaintIssue;

    private String complaintInputChannel;

    private String complaintClassificationLevel;


    private Integer overSla;

    private String responsibleBu1;

    private String responsibleBu2;

    private boolean isPicked;

    private Date openDate;

    private Integer sla;

    private Date activateDateTime;

    private Boolean isVerbatim;

    private Date loginDateTime;

    private Date logOutDateTime;

    private Date sellingDate;

}
