package com.atvantage.cxm.ccrpoutstanding.data;

import org.springframework.data.repository.CrudRepository;

public interface CcrpoutstandingRepository extends CrudRepository<CcrpOutstanding, String> {
}
