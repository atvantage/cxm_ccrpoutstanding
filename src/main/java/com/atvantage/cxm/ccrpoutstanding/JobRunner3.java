package com.atvantage.cxm.ccrpoutstanding;

import com.atvantage.cxm.ccrpoutstanding.data.CcrpOutstanding;
import com.atvantage.cxm.ccrpoutstanding.data.CcrpoutstandingRepository;
import com.atvantage.cxm.ccrpoutstanding.data.Transaction;
import com.atvantage.cxm.ccrpoutstanding.service.CCRPOutstandingService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import sun.util.calendar.CalendarUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Service
@Slf4j
public class JobRunner3 {
  private final CCRPOutstandingService ccrpOutstandingService;
  private final CcrpoutstandingRepository ccrpoutstandingRepository;
  private String dateText;
  private String fromZoneId;
  private String toZoneId;

  public JobRunner3(CCRPOutstandingService ccrpOutstandingService, CcrpoutstandingRepository ccrpoutstandingRepository) {
    this.ccrpOutstandingService = ccrpOutstandingService;
    this.ccrpoutstandingRepository = ccrpoutstandingRepository;
  }

  public void setDateText(String dateText) {
    this.dateText = dateText;
  }

  public void run(){
    TimeZone tz = TimeZone.getTimeZone("Asia/Bangkok");
    DateFormat TFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    TFormat.setTimeZone(tz);

    log.debug("inputDate = {}, fromZoneId={}, toZoneId={}",dateText,fromZoneId, toZoneId);
    LocalDate localDate = LocalDate.parse(dateText);
    LocalDateTime startDateTime = localDate.atStartOfDay();
    localDate = localDate.plusDays(1);
    LocalDateTime endDateTime = localDate.atStartOfDay();
    log.debug("startDateTime = {}, endDateTime = {} ",startDateTime, endDateTime);

    List<Transaction> transactions = ccrpOutstandingService.getCCRPTransactionWhereInputDateBetween(startDateTime, endDateTime);
    log.debug("transactions size = {}", transactions.size());

    transactions.forEach(t -> {

      if(t.getInputDate()!=null) t.setInputDate(Date.from( LocalDateTime.ofInstant(t.getInputDate().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));
      if(t.getUpdateDate()!=null) t.setUpdateDate(Date.from( LocalDateTime.ofInstant(t.getUpdateDate().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));
      if(t.getReceiveDateTime()!=null) t.setReceiveDateTime(Date.from( LocalDateTime.ofInstant(t.getReceiveDateTime().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));
      if(t.getCreateSurveyDate()!=null) t.setCreateSurveyDate(Date.from( LocalDateTime.ofInstant(t.getCreateSurveyDate().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));
      if(t.getInteractionDateTime()!=null) t.setInteractionDateTime(Date.from( LocalDateTime.ofInstant(t.getInteractionDateTime().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));
      if(t.getRequestDateTime()!=null) t.setRequestDateTime(Date.from( LocalDateTime.ofInstant(t.getRequestDateTime().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));
      if(t.getOpenDate()!=null) t.setOpenDate(Date.from( LocalDateTime.ofInstant(t.getOpenDate().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));
      if(t.getActivateDateTime()!=null) t.setActivateDateTime(Date.from( LocalDateTime.ofInstant(t.getActivateDateTime().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));
      if(t.getLoginDateTime()!=null) t.setLoginDateTime(Date.from( LocalDateTime.ofInstant(t.getLoginDateTime().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));
      if(t.getLogOutDateTime()!=null) t.setLogOutDateTime(Date.from( LocalDateTime.ofInstant(t.getLogOutDateTime().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));
      if(t.getSellingDate()!=null) t.setSellingDate(Date.from( LocalDateTime.ofInstant(t.getSellingDate().toInstant(), ZoneId.of("Asia/Bangkok")).toInstant(ZoneOffset.ofHours(0))));



      // Check does transaction have openDate field , if not skip this transaction
      if(t.getOpenDate() == null) {
        log.debug("Transaction sourceReferenceId = {}, inputDate= {} : not have openDate field, so skip from CCRPOutstanding conversion"
            , t.getSourceReferenceId(),t.getInputDate());
        return;
      }

      CcrpOutstanding c = new CcrpOutstanding();
      BeanUtils.copyProperties(t,c);
      log.debug("Transaction t.getSourceReferenceId()={},t.getInputDate()={}, t.getOpenDate()={}, t.getSla()={}"
          , t.getSourceReferenceId(),t.getInputDate(), t.getOpenDate(), t.getSla());



      //TODO must fix to openDate and sla , because it time of testing, no real data provided
//      c.setOpenDate(c.getInputDate());
//      c.setSla(3);

      c.setResponsibleChannel( getMappingKey(c.getResponsibleBuGrouping()));
      c.setResponsibleSubChannel( getMappingKey(c.getResponsibleBuDetails()));

      LocalDate overSlaCriteriaDate = c.getOpenDate().toInstant().atZone(ZoneId.of("UTC+00:00")).toLocalDate();
      overSlaCriteriaDate = overSlaCriteriaDate.plusDays(c.getSla());
      LocalDate currentDate = LocalDate.now();
      log.debug("overSlaCriteriaDate = {}, currentDate = {}", overSlaCriteriaDate, currentDate);
      c.setOverSla(overSlaCriteriaDate.isBefore(currentDate) ? overSlaCriteriaDate.until(currentDate).getDays() : 0);
      log.debug("overSla = {}", c.getOverSla());
      ccrpoutstandingRepository.save(c);
    });
  }

  private String getMappingKey(String channel) {

    if (StringUtils.isEmpty(channel)) {
      return channel;
    }

    if (channel.contains("BBG")) {
      return "BBG";
    }

    if (channel.contains("Contact Center")) {
      return "TMB Contact Center";
    }

    if (channel.contains("Inbound")) {
      return "Inbound";
    }

    if (channel.contains("Outbound")) {
      return "Outbound";
    }

    if (channel.contains("Direct Sales")) {
      return "Mobile";
    }

    if (channel.contains("Wealth")) {
      return "Wealth";
    }

    return channel;

  }

  public String getFromZoneId() {
    return fromZoneId;
  }

  public void setFromZoneId(String fromZoneId) {
    this.fromZoneId = fromZoneId;
  }

  public String getToZoneId() {
    return toZoneId;
  }

  public void setToZoneId(String toZoneId) {
    this.toZoneId = toZoneId;
  }
}
