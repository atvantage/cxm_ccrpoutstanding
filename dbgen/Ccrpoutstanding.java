package com.sample;


public class Ccrpoutstanding {

  private String id;
  private double accumulateProbability;
  private java.sql.Timestamp activateDate;
  private String activityDescription;
  private String activityHistory;
  private String activityTopUp;
  private String activityType;
  private String agentTeam;
  private String alertCategories;
  private String applicationVersion;
  private long assigned;
  private String branchCode;
  private String branchCodeRaw;
  private String branchName;
  private String businessOutcomeGrouping;
  private String callDriver;
  private String callLength;
  private String callWaiting;
  private String campaignCode;
  private String channel;
  private String channelScript;
  private String complaintClassificationLevel;
  private String complaintInputChannel;
  private String complaintIssue;
  private java.sql.Timestamp createSurveyDate;
  private String customerId;
  private String customerName;
  private String customerSegment;
  private String customerSubSegment;
  private String customerSubSegmentAbbreviation;
  private String customerSubSegmentDescription;
  private String customerType;
  private String deviceModel;
  private String entityType;
  private String expireFlag;
  private long fcr;
  private java.sql.Timestamp inputDate;
  private String inputUser;
  private String interactionCode;
  private java.sql.Timestamp interactionDate;
  private String interactionScript;
  private String interactionType;
  private String isPicked;
  private String language;
  private java.sql.Timestamp logInDate;
  private java.sql.Timestamp logOutDate;
  private String niceBranchCode;
  private String niceBranchName;
  private String niceRegionCode;
  private String niceZoneCode;
  private String nonProductIssue;
  private String optIn;
  private long overSla;
  private String parentBusinessOutcome;
  private String picked;
  private String platformUse;
  private double probability;
  private String productCategory;
  private String productCode;
  private String productGroup;
  private String productGroupAbbreviation;
  private String productGroupCode;
  private String productGroupDescription;
  private String productLevel;
  private String productName;
  private String productScript;
  private String productSubGroup;
  private String productSubGroupDescription;
  private long quota;
  private String quotaResult;
  private java.sql.Timestamp receiveDate;
  private String regionCode;
  private String regionCode2;
  private String regionCodeRaw;
  private String regionName;
  private String requestChannel;
  private java.sql.Timestamp requestDate;
  private String requestDescription;
  private String requestId;
  private String requestStatus;
  private String responseChannel;
  private String responsibleBu1;
  private String responsibleBu2;
  private String responsibleBuDetails;
  private String responsibleBuGrouping;
  private String resultStatus;
  private String retryTime;
  private String saleCode;
  private java.sql.Timestamp sellingDate;
  private String sequenceId;
  private String smsMessage;
  private String source;
  private String sourceReferenceId;
  private String spendTimeInSystem;
  private String staffId;
  private String status;
  private String subCategory;
  private String subChannel;
  private String subChannel1;
  private String subChannel2;
  private String subInteractionType;
  private long successCount;
  private String surveySet;
  private String surveyType;
  private String surveyUrl;
  private String transactionType;
  private String transactionTypeLevel;
  private java.sql.Timestamp updateDate;
  private String updateUser;
  private String wmCode;
  private String zoneCode;
  private String zoneCode2;
  private String zoneCodeRaw;
  private String zoneName;


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }


  public double getAccumulateProbability() {
    return accumulateProbability;
  }

  public void setAccumulateProbability(double accumulateProbability) {
    this.accumulateProbability = accumulateProbability;
  }


  public java.sql.Timestamp getActivateDate() {
    return activateDate;
  }

  public void setActivateDate(java.sql.Timestamp activateDate) {
    this.activateDate = activateDate;
  }


  public String getActivityDescription() {
    return activityDescription;
  }

  public void setActivityDescription(String activityDescription) {
    this.activityDescription = activityDescription;
  }


  public String getActivityHistory() {
    return activityHistory;
  }

  public void setActivityHistory(String activityHistory) {
    this.activityHistory = activityHistory;
  }


  public String getActivityTopUp() {
    return activityTopUp;
  }

  public void setActivityTopUp(String activityTopUp) {
    this.activityTopUp = activityTopUp;
  }


  public String getActivityType() {
    return activityType;
  }

  public void setActivityType(String activityType) {
    this.activityType = activityType;
  }


  public String getAgentTeam() {
    return agentTeam;
  }

  public void setAgentTeam(String agentTeam) {
    this.agentTeam = agentTeam;
  }


  public String getAlertCategories() {
    return alertCategories;
  }

  public void setAlertCategories(String alertCategories) {
    this.alertCategories = alertCategories;
  }


  public String getApplicationVersion() {
    return applicationVersion;
  }

  public void setApplicationVersion(String applicationVersion) {
    this.applicationVersion = applicationVersion;
  }


  public long getAssigned() {
    return assigned;
  }

  public void setAssigned(long assigned) {
    this.assigned = assigned;
  }


  public String getBranchCode() {
    return branchCode;
  }

  public void setBranchCode(String branchCode) {
    this.branchCode = branchCode;
  }


  public String getBranchCodeRaw() {
    return branchCodeRaw;
  }

  public void setBranchCodeRaw(String branchCodeRaw) {
    this.branchCodeRaw = branchCodeRaw;
  }


  public String getBranchName() {
    return branchName;
  }

  public void setBranchName(String branchName) {
    this.branchName = branchName;
  }


  public String getBusinessOutcomeGrouping() {
    return businessOutcomeGrouping;
  }

  public void setBusinessOutcomeGrouping(String businessOutcomeGrouping) {
    this.businessOutcomeGrouping = businessOutcomeGrouping;
  }


  public String getCallDriver() {
    return callDriver;
  }

  public void setCallDriver(String callDriver) {
    this.callDriver = callDriver;
  }


  public String getCallLength() {
    return callLength;
  }

  public void setCallLength(String callLength) {
    this.callLength = callLength;
  }


  public String getCallWaiting() {
    return callWaiting;
  }

  public void setCallWaiting(String callWaiting) {
    this.callWaiting = callWaiting;
  }


  public String getCampaignCode() {
    return campaignCode;
  }

  public void setCampaignCode(String campaignCode) {
    this.campaignCode = campaignCode;
  }


  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }


  public String getChannelScript() {
    return channelScript;
  }

  public void setChannelScript(String channelScript) {
    this.channelScript = channelScript;
  }


  public String getComplaintClassificationLevel() {
    return complaintClassificationLevel;
  }

  public void setComplaintClassificationLevel(String complaintClassificationLevel) {
    this.complaintClassificationLevel = complaintClassificationLevel;
  }


  public String getComplaintInputChannel() {
    return complaintInputChannel;
  }

  public void setComplaintInputChannel(String complaintInputChannel) {
    this.complaintInputChannel = complaintInputChannel;
  }


  public String getComplaintIssue() {
    return complaintIssue;
  }

  public void setComplaintIssue(String complaintIssue) {
    this.complaintIssue = complaintIssue;
  }


  public java.sql.Timestamp getCreateSurveyDate() {
    return createSurveyDate;
  }

  public void setCreateSurveyDate(java.sql.Timestamp createSurveyDate) {
    this.createSurveyDate = createSurveyDate;
  }


  public String getCustomerId() {
    return customerId;
  }

  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }


  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }


  public String getCustomerSegment() {
    return customerSegment;
  }

  public void setCustomerSegment(String customerSegment) {
    this.customerSegment = customerSegment;
  }


  public String getCustomerSubSegment() {
    return customerSubSegment;
  }

  public void setCustomerSubSegment(String customerSubSegment) {
    this.customerSubSegment = customerSubSegment;
  }


  public String getCustomerSubSegmentAbbreviation() {
    return customerSubSegmentAbbreviation;
  }

  public void setCustomerSubSegmentAbbreviation(String customerSubSegmentAbbreviation) {
    this.customerSubSegmentAbbreviation = customerSubSegmentAbbreviation;
  }


  public String getCustomerSubSegmentDescription() {
    return customerSubSegmentDescription;
  }

  public void setCustomerSubSegmentDescription(String customerSubSegmentDescription) {
    this.customerSubSegmentDescription = customerSubSegmentDescription;
  }


  public String getCustomerType() {
    return customerType;
  }

  public void setCustomerType(String customerType) {
    this.customerType = customerType;
  }


  public String getDeviceModel() {
    return deviceModel;
  }

  public void setDeviceModel(String deviceModel) {
    this.deviceModel = deviceModel;
  }


  public String getEntityType() {
    return entityType;
  }

  public void setEntityType(String entityType) {
    this.entityType = entityType;
  }


  public String getExpireFlag() {
    return expireFlag;
  }

  public void setExpireFlag(String expireFlag) {
    this.expireFlag = expireFlag;
  }


  public long getFcr() {
    return fcr;
  }

  public void setFcr(long fcr) {
    this.fcr = fcr;
  }


  public java.sql.Timestamp getInputDate() {
    return inputDate;
  }

  public void setInputDate(java.sql.Timestamp inputDate) {
    this.inputDate = inputDate;
  }


  public String getInputUser() {
    return inputUser;
  }

  public void setInputUser(String inputUser) {
    this.inputUser = inputUser;
  }


  public String getInteractionCode() {
    return interactionCode;
  }

  public void setInteractionCode(String interactionCode) {
    this.interactionCode = interactionCode;
  }


  public java.sql.Timestamp getInteractionDate() {
    return interactionDate;
  }

  public void setInteractionDate(java.sql.Timestamp interactionDate) {
    this.interactionDate = interactionDate;
  }


  public String getInteractionScript() {
    return interactionScript;
  }

  public void setInteractionScript(String interactionScript) {
    this.interactionScript = interactionScript;
  }


  public String getInteractionType() {
    return interactionType;
  }

  public void setInteractionType(String interactionType) {
    this.interactionType = interactionType;
  }


  public String getIsPicked() {
    return isPicked;
  }

  public void setIsPicked(String isPicked) {
    this.isPicked = isPicked;
  }


  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }


  public java.sql.Timestamp getLogInDate() {
    return logInDate;
  }

  public void setLogInDate(java.sql.Timestamp logInDate) {
    this.logInDate = logInDate;
  }


  public java.sql.Timestamp getLogOutDate() {
    return logOutDate;
  }

  public void setLogOutDate(java.sql.Timestamp logOutDate) {
    this.logOutDate = logOutDate;
  }


  public String getNiceBranchCode() {
    return niceBranchCode;
  }

  public void setNiceBranchCode(String niceBranchCode) {
    this.niceBranchCode = niceBranchCode;
  }


  public String getNiceBranchName() {
    return niceBranchName;
  }

  public void setNiceBranchName(String niceBranchName) {
    this.niceBranchName = niceBranchName;
  }


  public String getNiceRegionCode() {
    return niceRegionCode;
  }

  public void setNiceRegionCode(String niceRegionCode) {
    this.niceRegionCode = niceRegionCode;
  }


  public String getNiceZoneCode() {
    return niceZoneCode;
  }

  public void setNiceZoneCode(String niceZoneCode) {
    this.niceZoneCode = niceZoneCode;
  }


  public String getNonProductIssue() {
    return nonProductIssue;
  }

  public void setNonProductIssue(String nonProductIssue) {
    this.nonProductIssue = nonProductIssue;
  }


  public String getOptIn() {
    return optIn;
  }

  public void setOptIn(String optIn) {
    this.optIn = optIn;
  }


  public long getOverSla() {
    return overSla;
  }

  public void setOverSla(long overSla) {
    this.overSla = overSla;
  }


  public String getParentBusinessOutcome() {
    return parentBusinessOutcome;
  }

  public void setParentBusinessOutcome(String parentBusinessOutcome) {
    this.parentBusinessOutcome = parentBusinessOutcome;
  }


  public String getPicked() {
    return picked;
  }

  public void setPicked(String picked) {
    this.picked = picked;
  }


  public String getPlatformUse() {
    return platformUse;
  }

  public void setPlatformUse(String platformUse) {
    this.platformUse = platformUse;
  }


  public double getProbability() {
    return probability;
  }

  public void setProbability(double probability) {
    this.probability = probability;
  }


  public String getProductCategory() {
    return productCategory;
  }

  public void setProductCategory(String productCategory) {
    this.productCategory = productCategory;
  }


  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }


  public String getProductGroup() {
    return productGroup;
  }

  public void setProductGroup(String productGroup) {
    this.productGroup = productGroup;
  }


  public String getProductGroupAbbreviation() {
    return productGroupAbbreviation;
  }

  public void setProductGroupAbbreviation(String productGroupAbbreviation) {
    this.productGroupAbbreviation = productGroupAbbreviation;
  }


  public String getProductGroupCode() {
    return productGroupCode;
  }

  public void setProductGroupCode(String productGroupCode) {
    this.productGroupCode = productGroupCode;
  }


  public String getProductGroupDescription() {
    return productGroupDescription;
  }

  public void setProductGroupDescription(String productGroupDescription) {
    this.productGroupDescription = productGroupDescription;
  }


  public String getProductLevel() {
    return productLevel;
  }

  public void setProductLevel(String productLevel) {
    this.productLevel = productLevel;
  }


  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }


  public String getProductScript() {
    return productScript;
  }

  public void setProductScript(String productScript) {
    this.productScript = productScript;
  }


  public String getProductSubGroup() {
    return productSubGroup;
  }

  public void setProductSubGroup(String productSubGroup) {
    this.productSubGroup = productSubGroup;
  }


  public String getProductSubGroupDescription() {
    return productSubGroupDescription;
  }

  public void setProductSubGroupDescription(String productSubGroupDescription) {
    this.productSubGroupDescription = productSubGroupDescription;
  }


  public long getQuota() {
    return quota;
  }

  public void setQuota(long quota) {
    this.quota = quota;
  }


  public String getQuotaResult() {
    return quotaResult;
  }

  public void setQuotaResult(String quotaResult) {
    this.quotaResult = quotaResult;
  }


  public java.sql.Timestamp getReceiveDate() {
    return receiveDate;
  }

  public void setReceiveDate(java.sql.Timestamp receiveDate) {
    this.receiveDate = receiveDate;
  }


  public String getRegionCode() {
    return regionCode;
  }

  public void setRegionCode(String regionCode) {
    this.regionCode = regionCode;
  }


  public String getRegionCode2() {
    return regionCode2;
  }

  public void setRegionCode2(String regionCode2) {
    this.regionCode2 = regionCode2;
  }


  public String getRegionCodeRaw() {
    return regionCodeRaw;
  }

  public void setRegionCodeRaw(String regionCodeRaw) {
    this.regionCodeRaw = regionCodeRaw;
  }


  public String getRegionName() {
    return regionName;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }


  public String getRequestChannel() {
    return requestChannel;
  }

  public void setRequestChannel(String requestChannel) {
    this.requestChannel = requestChannel;
  }


  public java.sql.Timestamp getRequestDate() {
    return requestDate;
  }

  public void setRequestDate(java.sql.Timestamp requestDate) {
    this.requestDate = requestDate;
  }


  public String getRequestDescription() {
    return requestDescription;
  }

  public void setRequestDescription(String requestDescription) {
    this.requestDescription = requestDescription;
  }


  public String getRequestId() {
    return requestId;
  }

  public void setRequestId(String requestId) {
    this.requestId = requestId;
  }


  public String getRequestStatus() {
    return requestStatus;
  }

  public void setRequestStatus(String requestStatus) {
    this.requestStatus = requestStatus;
  }


  public String getResponseChannel() {
    return responseChannel;
  }

  public void setResponseChannel(String responseChannel) {
    this.responseChannel = responseChannel;
  }


  public String getResponsibleBu1() {
    return responsibleBu1;
  }

  public void setResponsibleBu1(String responsibleBu1) {
    this.responsibleBu1 = responsibleBu1;
  }


  public String getResponsibleBu2() {
    return responsibleBu2;
  }

  public void setResponsibleBu2(String responsibleBu2) {
    this.responsibleBu2 = responsibleBu2;
  }


  public String getResponsibleBuDetails() {
    return responsibleBuDetails;
  }

  public void setResponsibleBuDetails(String responsibleBuDetails) {
    this.responsibleBuDetails = responsibleBuDetails;
  }


  public String getResponsibleBuGrouping() {
    return responsibleBuGrouping;
  }

  public void setResponsibleBuGrouping(String responsibleBuGrouping) {
    this.responsibleBuGrouping = responsibleBuGrouping;
  }


  public String getResultStatus() {
    return resultStatus;
  }

  public void setResultStatus(String resultStatus) {
    this.resultStatus = resultStatus;
  }


  public String getRetryTime() {
    return retryTime;
  }

  public void setRetryTime(String retryTime) {
    this.retryTime = retryTime;
  }


  public String getSaleCode() {
    return saleCode;
  }

  public void setSaleCode(String saleCode) {
    this.saleCode = saleCode;
  }


  public java.sql.Timestamp getSellingDate() {
    return sellingDate;
  }

  public void setSellingDate(java.sql.Timestamp sellingDate) {
    this.sellingDate = sellingDate;
  }


  public String getSequenceId() {
    return sequenceId;
  }

  public void setSequenceId(String sequenceId) {
    this.sequenceId = sequenceId;
  }


  public String getSmsMessage() {
    return smsMessage;
  }

  public void setSmsMessage(String smsMessage) {
    this.smsMessage = smsMessage;
  }


  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }


  public String getSourceReferenceId() {
    return sourceReferenceId;
  }

  public void setSourceReferenceId(String sourceReferenceId) {
    this.sourceReferenceId = sourceReferenceId;
  }


  public String getSpendTimeInSystem() {
    return spendTimeInSystem;
  }

  public void setSpendTimeInSystem(String spendTimeInSystem) {
    this.spendTimeInSystem = spendTimeInSystem;
  }


  public String getStaffId() {
    return staffId;
  }

  public void setStaffId(String staffId) {
    this.staffId = staffId;
  }


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }


  public String getSubCategory() {
    return subCategory;
  }

  public void setSubCategory(String subCategory) {
    this.subCategory = subCategory;
  }


  public String getSubChannel() {
    return subChannel;
  }

  public void setSubChannel(String subChannel) {
    this.subChannel = subChannel;
  }


  public String getSubChannel1() {
    return subChannel1;
  }

  public void setSubChannel1(String subChannel1) {
    this.subChannel1 = subChannel1;
  }


  public String getSubChannel2() {
    return subChannel2;
  }

  public void setSubChannel2(String subChannel2) {
    this.subChannel2 = subChannel2;
  }


  public String getSubInteractionType() {
    return subInteractionType;
  }

  public void setSubInteractionType(String subInteractionType) {
    this.subInteractionType = subInteractionType;
  }


  public long getSuccessCount() {
    return successCount;
  }

  public void setSuccessCount(long successCount) {
    this.successCount = successCount;
  }


  public String getSurveySet() {
    return surveySet;
  }

  public void setSurveySet(String surveySet) {
    this.surveySet = surveySet;
  }


  public String getSurveyType() {
    return surveyType;
  }

  public void setSurveyType(String surveyType) {
    this.surveyType = surveyType;
  }


  public String getSurveyUrl() {
    return surveyUrl;
  }

  public void setSurveyUrl(String surveyUrl) {
    this.surveyUrl = surveyUrl;
  }


  public String getTransactionType() {
    return transactionType;
  }

  public void setTransactionType(String transactionType) {
    this.transactionType = transactionType;
  }


  public String getTransactionTypeLevel() {
    return transactionTypeLevel;
  }

  public void setTransactionTypeLevel(String transactionTypeLevel) {
    this.transactionTypeLevel = transactionTypeLevel;
  }


  public java.sql.Timestamp getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(java.sql.Timestamp updateDate) {
    this.updateDate = updateDate;
  }


  public String getUpdateUser() {
    return updateUser;
  }

  public void setUpdateUser(String updateUser) {
    this.updateUser = updateUser;
  }


  public String getWmCode() {
    return wmCode;
  }

  public void setWmCode(String wmCode) {
    this.wmCode = wmCode;
  }


  public String getZoneCode() {
    return zoneCode;
  }

  public void setZoneCode(String zoneCode) {
    this.zoneCode = zoneCode;
  }


  public String getZoneCode2() {
    return zoneCode2;
  }

  public void setZoneCode2(String zoneCode2) {
    this.zoneCode2 = zoneCode2;
  }


  public String getZoneCodeRaw() {
    return zoneCodeRaw;
  }

  public void setZoneCodeRaw(String zoneCodeRaw) {
    this.zoneCodeRaw = zoneCodeRaw;
  }


  public String getZoneName() {
    return zoneName;
  }

  public void setZoneName(String zoneName) {
    this.zoneName = zoneName;
  }

}
